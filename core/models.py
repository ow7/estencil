from django.db import models
from django.utils.translation import ugettext_lazy as _


STATE_CHOICES = (
    ('AC', u'Acre'),
    ('AL', u'Alagoas'),
    ('AP', u'Amapá'),
    ('AM', u'Amazonas'),
    ('BA', u'Bahia'),
    ('CE', u'Ceará'),
    ('DF', u'Distrito Federal'),
    ('ES', u'Espírito Santo'),
    ('GO', u'Goiás'),
    ('MA', u'Maranhão'),
    ('MT', u'Mato Grosso'),
    ('MS', u'Mato Grosso do Sul'),
    ('MG', u'Minas Gerais'),
    ('PA', u'Pará'),
    ('PB', u'Paraíba'),
    ('PR', u'Paraná'),
    ('PE', u'Pernambuco'),
    ('PI', u'Piauí'),
    ('RJ', u'Rio de Janeiro'),
    ('RN', u'Rio Grande do Norte'),
    ('RS', u'Rio Grande do Sul'),
    ('RO', u'Rondônia'),
    ('RR', u'Roraima'),
    ('SC', u'Santa Catarina'),
    ('SP', u'São Paulo'),
    ('SE', u'Sergipe'),
    ('TO', u'Tocantins'),
)

SOCIAL_CHOICES = (
    ('behance', _(u'Behance')),
    ('dribbble', _(u'Dribbble')),
    ('facebook-f', _(u'Facebook')),
    ('flickr', _(u'Flickr')),
    ('google', _(u'Google-plus')),
    ('instagram', _(u'Instagram')),
    ('linkedin', _(u'Linkedin')),
    ('reddit', _(u'Reddit')),
    ('scribd', _(u'Scribd')),
    ('slack', _(u'Slack')),
    ('snapchat', _(u'Snapchat')),
    ('soundcloud', _(u'Soundcloud')),
    ('twitter', _(u'Twitter')),
    ('vimeo', _(u'Vimeo')),
    ('vine', _(u'Vine')),
    ('youtube', _('Youtube')),
    ('pinterest', _('Pinterest')),
    ('whatsapp', _('Whatsapp')),
)

CTA_URL = (
    ('#header', _(u'Home')),
    ('#services', _(u'Serviços')),
    ('#why', _(u'Porque Nós')),
    ('#reviews', _(u'Avaliações')),
    ('#estimate', _(u'Orçamento')),
    ('#external', _(u'URL Externa')),
)


class Social(models.Model):
    name = models.CharField(
        _(u'Nome'), max_length=30, choices=SOCIAL_CHOICES)
    link = models.URLField(
        _(u'Link'), help_text='Ex: http://www.facebook.com/usuario')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = _(u'Mídias Sociais')
        verbose_name = _(u'Mídia Social')


class Banner(models.Model):
    image = models.ImageField(
        _(u'Imagem'), upload_to='banners')
    title = models.CharField(
        _(u'Título'), max_length=100)
    intro = models.CharField(
        _(u'Introdução'), max_length=100)
    btn_cta_title = models.CharField(
        _(u'Botão: Título'), max_length=15)
    btn_cta_url = models.CharField(
        _(u'Botão: URL'), max_length=20, choices=CTA_URL)
    btn_cta_external = models.CharField(
        _(u'Botão: URL Externa'), max_length=100, blank=True, null=True)
    published = models.BooleanField(
        _(u'Publicar?'), default=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name_plural = _(u'Banners')
        verbose_name = _(u'Banner')


class Service(models.Model):
    image = models.ImageField(
        _(u'Imagem'), upload_to='services')
    title = models.CharField(
        _(u'Título'), max_length=100)
    slug = models.SlugField(
        _(u'URL no Site'), max_length=100, db_index=True)
    desc = models.CharField(
        _(u'Descrição'), max_length=200, blank=True, null=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name_plural = _(u'Serviços')
        verbose_name = _(u'Serviço')


class Why(models.Model):
    title = models.CharField(
        _(u'Título'), max_length=100)
    intro = models.CharField(
        _(u'Introdução'), max_length=200)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name_plural = _(u'Porque nós?')
        verbose_name = _(u'Porque nós?')


class Review(models.Model):
    name = models.CharField(
        _(u'Nome'), max_length=30)
    comment = models.TextField(
        _(u'Comentário'))

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = _(u'Avaliações')
        verbose_name = _(u'Avaliação')


class HomePage(models.Model):
    title = models.CharField(
        _(u'Título da Página'), max_length=100)
    banners = models.ManyToManyField(
        'Banner', verbose_name=_(u'Banners'))
    service_title = models.CharField(
        _(u'Serviços: Título'), max_length=50)
    service_intro = models.TextField(
        _(u'Serviços: Introdução'))
    why_title = models.CharField(
        _(u'Porque: Título'), max_length=50)
    why_intro = models.TextField(
        _(u'Proque: Introdução'))
    review_title = models.CharField(
        _(u'Avaliações: Título'), max_length=50)
    estimate_title = models.CharField(
        _(u'Orçamento: Título'), max_length=50)
    estimate_intro = models.TextField(
        _(u'Orçamento: Introdução'))
    start_page = models.BooleanField(
        _(u'Página Principal?'), default=False)


    def __str__(self):
        return self.title

    class Meta:
        verbose_name_plural = _(u'Páginas')
        verbose_name = _(u'Página')


class Company(models.Model):
    image = models.ImageField(
        _(u'Logo'), upload_to='company')
    name = models.CharField(
        _(u'Nome'), max_length=100)
    description = models.CharField(
        _(u'Descrição'), max_length=100, blank=True, null=True)
    cnpj = models.CharField(
        _(u'CNPJ'), max_length=20, help_text='99.999.999/9999-99')
    address = models.CharField(
        _(u'Rua e Número'), max_length=200)
    complement = models.CharField(
        _(u'Complemento'), max_length=100, blank=True, null=True)
    cep = models.CharField(
        _(u'CEP'), max_length=9, help_text='99999-999', blank=True, null=True)
    district = models.CharField(
        _(u'Bairro'), max_length=100)
    city = models.CharField(
        _(u'Cidade'), max_length=100)
    state = models.CharField(
        _(u'UF'), max_length=2, choices=STATE_CHOICES)
    country = models.CharField(
        _(u'País'), max_length=50, default="BR")
    phone1 = models.CharField(
        _(u'Fone 1'), max_length=20, help_text='(99) 9 9999-9999')
    phone1_wa = models.BooleanField(
        _(u'Fone 1 é Whatsapp?'), default=True)
    phone2 = models.CharField(
        _(u'Fone 2'), max_length=20, help_text='(99) 9 9999-9999', blank=True,
        null=True)
    phone2_wa = models.BooleanField(
        _(u'Fone 2 é Whatsapp?'), default=False)
    phone3 = models.CharField(
        _(u'Fone 3'), max_length=20, help_text='(99) 9 9999-9999', blank=True,
        null=True)
    phone3_wa = models.BooleanField(
        _(u'Fone 3 é Whatsapp?'), default=False)
    phone4 = models.CharField(
        _(u'Fone 4'), max_length=20, help_text='(99) 9 9999-9999', blank=True,
        null=True)
    phone4_wa = models.BooleanField(
        _(u'Fone 4 é Whatsapp?'), default=False)
    email = models.EmailField(
        _(u'Email'))
    site = models.URLField(
        _(u'Site'))
    socials = models.ManyToManyField(
        'Social', verbose_name=_(u'Mídias Sociais'))

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = _(u'Empresa')
        verbose_name = _(u'Empresa')


class Estimate(models.Model):
    service = models.ForeignKey(
        'Service', on_delete=models.CASCADE, related_name='estimate_service',
        verbose_name=_('Serviço'), blank=True, null=True)
    name = models.CharField(
        _(u'Nome'), max_length=100)
    email = models.EmailField(
        _(u'Email'))
    phone = models.CharField(
        _(u'Fone'), max_length=20)
    desc = models.TextField(
        _(u'Descrição'))
    done = models.BooleanField(
        _(u'Concluído?'), default=False)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('-created',)
        verbose_name_plural = _(u'Orçamentos')
        verbose_name = _(u'Orçamento')
