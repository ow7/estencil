# Generated by Django 2.0.5 on 2018-08-30 14:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0005_homepage_start_page'),
    ]

    operations = [
        migrations.CreateModel(
            name='Service',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image', models.ImageField(upload_to='services', verbose_name='Imagem')),
                ('title', models.CharField(max_length=100, verbose_name='Título')),
                ('intro', models.CharField(max_length=100, verbose_name='Introdução')),
            ],
            options={
                'verbose_name': 'Serviço',
                'verbose_name_plural': 'Serviços',
            },
        ),
        migrations.AlterField(
            model_name='banner',
            name='image',
            field=models.ImageField(upload_to='banners', verbose_name='Imagem'),
        ),
    ]
