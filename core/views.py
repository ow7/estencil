import json

from django.conf import settings
from django.template.loader import render_to_string
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect, HttpResponse
from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from django.core.mail import EmailMultiAlternatives

from core.models import (Banner, Company, HomePage, Social, Why, Review,
                         Service, Estimate)
from core.forms import EstimateForm


def home(request):
    page = get_object_or_404(HomePage, start_page=True)
    company = get_object_or_404(Company, pk=1)
    services = Service.objects.all()
    whys = Why.objects.all()
    reviews = Review.objects.all()

    context = {
        'page': page,
        'company': company,
        'services': services,
        'whys': whys,
        'reviews': reviews,
        'form': EstimateForm(),
    }

    return render(request, 'index.html', context)


def create_estimate(request):
    print(request)
    if request.method == 'POST':
        company = get_object_or_404(Company, pk=1)

        service = request.POST.get('id_service', None)
        name = request.POST.get('id_name', None)
        email = request.POST.get('id_email', None)
        phone = request.POST.get('id_phone', None)
        desc = request.POST.get('id_desc', None)

        obj_service = Service.objects.get(id=service)

        response_data = {}
        response_data['service'] = obj_service
        response_data['name'] = name
        response_data['email'] = email
        response_data['phone'] = phone
        response_data['desc'] = desc

        if not name:
            response_data['result'] = 'Preencha seu nome!'
        elif not email:
            response_data['result'] = 'Preencha seu email!'
        elif not phone:
            response_data['result'] = 'Preencha seu fone!'
        elif not desc:
            response_data['result'] = 'Preencha a descrição!'
        else:
            est = Estimate(
                service=obj_service,
                name=name, email=email,
                phone=phone, desc=desc
            )
            est.save()

            _send_mail(
                '[Site] Orçamento para: {}'.format(obj_service.title),
                response_data['email'],
                company.email,
                'estimate_email.txt',
                'estimate_email.html',
                response_data
            )

            _send_mail(
                'Recebemos a sua solicitação de orçamento',
                company.email,
                response_data['email'],
                'estimate_email_ty.txt',
                'estimate_email_ty.html',
                response_data
            )

            response_data['result'] = 'Orçamento solicitado com sucesso!'
            response_data['service'] = 1
            response_data['name'] = ''
            response_data['email'] = ''
            response_data['phone'] = ''
            response_data['desc'] = ''

        return HttpResponse(
            json.dumps(response_data),
            content_type="application/json"
        )
    else:
        return HttpResponse(
            json.dumps({"nothing to see": "this isn't happening"}),
            content_type="application/json"
        )


def _send_mail(subject, from_, to, template_text, template_html, context):
    text_content = render_to_string(template_text, context)
    html_content = render_to_string(template_html, context)
    msg = EmailMultiAlternatives(subject, text_content, from_, [to])
    msg.attach_alternative(html_content, "text/html")
    msg.send()


def zoho(request):
    return render(request, "verifyforzoho.html")
