from django.contrib import admin

from core.models import (Banner, Company, HomePage, Social, Why,
                         Review, Service, Estimate)


admin.site.register(Banner)
admin.site.register(Company)
admin.site.register(HomePage)
admin.site.register(Social)
admin.site.register(Why)
admin.site.register(Review)


class EstimateAdmin(admin.ModelAdmin):
    list_filter = ('service', 'done')
    list_display = ('service', 'name', 'email', 'phone', 'done', 'created')
    list_editable = ('done',)
    search_fields = ('name', 'email', 'phone', 'desc')
    date_hierarchy = 'created'


admin.site.register(Estimate, EstimateAdmin)


class ServiceAdmin(admin.ModelAdmin):
    list_display = ('title', 'desc')
    search_fields = ('title', 'desc')
    prepopulated_fields = {'slug': ('title',)}


admin.site.register(Service, ServiceAdmin)
