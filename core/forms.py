from django import forms
from django.conf import settings
from django.template.loader import render_to_string
from django.core.mail import EmailMultiAlternatives

from core.models import Estimate


class EstimateForm(forms.ModelForm):

    class Meta:
        model = Estimate
        fields = ['service', 'name', 'email', 'phone', 'desc']
        widgets = {
            # 'service': forms.TextInput(
            #     attrs={
            #         'required': True,
            #         'placeholder': 'Serviço',
            #         'class': 'form-control'
            #     }
            # ),
            'name': forms.TextInput(
                attrs={
                    'required': True,
                    'placeholder': 'Nome',
                    'class': 'form-control'
                }
            ),
            'email': forms.EmailInput(
                attrs={
                    'required': True,
                    'placeholder': 'Email',
                    'class': 'form-control'
                }
            ),
            'phone': forms.TextInput(
                attrs={
                    'required': True,
                    'placeholder': 'Fone',
                    'class': 'form-control'
                }
            ),
            'desc': forms.Textarea(
                attrs={
                    'required': True,
                    'placeholder': 'Fone',
                    'class': 'form-control',
                    'rows': 6
                }
            ),
        }

        def __init__(self, user, *args, **kwargs):
            super(ServiceForm, self).__init__(*args, **kwargs)
            self.fields['service'].queryset = Service.objects.all()


    def send_mail(self):
        subject = u'Site - Orçamento para: %s'.format(self.cleaned_data['name'])
        context = {
            'service': self.cleaned_data['service'],
            'name': self.cleaned_data['name'],
            'email': self.cleaned_data['email'],
            'phone': self.cleaned_data['phone'],
            'desc': self.cleaned_data['desc'],
        }

        email_to = settings.DEFAULT_FROM_EMAIL

        message = render_to_string('estimate_mail.txt', context)
        message_html = render_to_string('estimate_mail.html', context)
        msg = EmailMultiAlternatives(
            subject, message, email_to, [email_to]
        )

        msg.attach_alternative(message_html, 'text/html')
        msg.send()
